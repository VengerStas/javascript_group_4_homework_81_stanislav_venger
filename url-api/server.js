const express = require('express');
const cors = require('cors');
const mongoose = require('mongoose');
const Url = require('./models/Urls');
const nanoid = require('nanoid');


const app = express();
app.use(cors());
app.use(express.json());

const port = 8000;

mongoose.connect('mongodb://localhost/urls', { useNewUrlParser: true }).then(() => {
    app.get('/:short', (req, res) => {
       Url.findOne({short: req.params.short})
           .then(originalUrl => {
               if (originalUrl) res.status(301).redirect(originalUrl.original)
           })
           .catch(error => res.status(400).send(error));
    });

    app.post('/', (req, res) => {
       const short = new Url({original: req.body.original, short: nanoid(7)});
       short.save()
           .then((result) => {
               res.send(result);
           })
           .catch(error => res.status(400).send(error));
    });

    app.listen(port, () => {
        console.log(`Server started on ${port} port`);
    })
});