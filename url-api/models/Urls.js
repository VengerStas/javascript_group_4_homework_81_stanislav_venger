const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const UrlSchema = new Schema ({
    original: {
        type: String,
        required: true
    },
    short: {
        type: String,
        unique: true,
        required: true
    },
});

const Url = mongoose.model('Url', UrlSchema);

module.exports = Url;