import React, {Component} from 'react';
import {connect} from "react-redux";
import {fetchPostUrl} from "../../store/actions";

import './Form.css';

class Form extends Component {
    state = {
        original: ''
    };

    changeValue = event => {
        this.setState ({
            [event.target.name]: event.target.value
        });
    };

    submitUrlHandler = event => {
        event.preventDefault();
        const data = {
            original: this.state.original
        };
        this.props.addUrl(data);
    };

    render() {
        return (
            <div className="block">
                <div className="block-url">
                    <form onSubmit={this.submitUrlHandler}>
                        <h4>Shorten your link!</h4>
                        <input type="text" name="original" value={this.state.original} onChange={this.changeValue} placeholder="Enter URL here"/>
                        <button type="submit">Shorten</button>
                    </form>
                    {this.props.urls.short ? <div className="url"><a href={this.props.urls.original}>http://localhost:8000/{this.props.urls.short}</a></div> : null}
                </div>
            </div>
        );
    }
}



const mapStateToProps = state => ({
    urls: state.urls
});

const mapDispatchToProps = dispatch => ({
    addUrl: data => dispatch(fetchPostUrl(data))
});

export default connect(mapStateToProps, mapDispatchToProps)(Form);