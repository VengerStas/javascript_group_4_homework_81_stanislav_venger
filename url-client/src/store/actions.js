import axios from '../axios-Url';

export const URL_REQUEST_SUCCESS = "URL_REQUEST_SUCCESS";

export const urlSuccess = urls => {
    return {type: URL_REQUEST_SUCCESS, urls};
};

export const fetchPostUrl= data => {
    return (dispatch) => {
        return axios.post('/', data).then((response) => {
            dispatch(urlSuccess(response.data))
        })
    }
};