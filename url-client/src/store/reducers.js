import {URL_REQUEST_SUCCESS} from "./actions";

const initialState = {
    urls: [],
};

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case URL_REQUEST_SUCCESS:
            return {
                ...state,
                urls: action.urls
            };
        default:
            return state;
    }
};

export default reducer;